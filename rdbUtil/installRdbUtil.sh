#!/bin/bash
# Script to install rdbUtil

SM_INIT_PATH="/home/ubuntu/rdbUtil"

# remove old
sudo rm $SM_INIT_PATH

# get new
/usr/bin/wget -O "$SM_INIT_PATH" "https://gitlab.com/Gugabit/tools/-/raw/main/rdbUtil/rdbUtil"

# mark as executable
sudo chmod +x $SM_INIT_PATH
