#!/bin/bash

DATA="$( (									\
	df;									\
	echo "---break----------------------------------------------------";	\
	ls -l /var/opt/mssql/data/;						\
	echo "---break----------------------------------------------------";	\
	systemctl status mssql-server;						\
	echo "---break----------------------------------------------------";	\
	free;									\
	echo "---break----------------------------------------------------";	\
	uptime;									\
	echo "---break----------------------------------------------------";	\
	cat /etc/hostname;                                                      \
	echo "---break----------------------------------------------------";	\
	curl http://checkip.amazonaws.com;
) | tr '\r\n' ' ')"

curl -k --location --request POST "https://aws-monitor.clinic.inf.br/aws-monitor/status-set/$CUSTOMER" \
--header 'Content-Type: application/json' \
--data-raw "{\"status\":\"$DATA\"}"
