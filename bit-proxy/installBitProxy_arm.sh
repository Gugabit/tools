#!/bin/bash
# Script to install bit-proxy as linux service

SM_INIT_PATH="/opt/bit-proxy/bit-proxy"

sudo mkdir -p /opt/bit-proxy

# remove old
sudo /bin/systemctl stop bit-proxy
sudo rm $SM_INIT_PATH

/usr/bin/wget -O "$SM_INIT_PATH" "https://gitlab.com/Gugabit/tools/-/raw/main/bit-proxy/bit-proxy_arm64"

sudo chmod +x $SM_INIT_PATH

echo "[Unit]
Description=Bit Proxy

[Service]
User=root
WorkingDirectory=/opt/bit-proxy
EnvironmentFile=/etc/environment
ExecStart=/opt/bit-proxy/bit-proxy

SuccessExitStatus=0
TimeoutStopSec=10
Restart=on-failure
RestartSec=5

StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=serversmonitorservice

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/bit-proxy.service

sudo /bin/systemctl enable bit-proxy

sudo /bin/systemctl start bit-proxy

sudo ufw allow 8090
sudo firewall-cmd --permanent --add-port=8090/tcp
sudo firewall-cmd --add-port=8090/tcp